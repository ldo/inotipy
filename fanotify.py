"""
Pure-Python binding for the Linux fanotify(7) API using ctypes and
working with asyncio.
"""
#+
# Copyright 2024 Lawrence D'Oliveiro <ldo@geek-central.gen.nz>.
# Licensed under the GNU Lesser General Public License v2.1 or later.
#-

import os
import enum
from functools import \
    reduce
import operator
import ctypes as ct
import struct
from weakref import \
    ref as weak_ref, \
    WeakValueDictionary
import asyncio
import atexit
import linuxfs

#+
# Low-level interface
#-

# following comes from /usr/include/bits/fcntl-linux.h or /usr/include/linux/fcntl.h
AT_FDCWD = -100 # special fd value indicating current working directory

# from /usr/include/asm-generic/posix_types.h:
# don’t bother with prefix underscores
class kernel_fsid_t(ct.Structure) :
    _fields_ = \
        [
            ("val", 2 * ct.c_int),
        ]
#end kernel_fsid_t

class FANOTIFY :
    "definitions of flag bits and structs that you will need."

    # event flags
    ACCESS = 0x00000001
    MODIFY = 0x00000002
    ATTRIB = 0x00000004
    CLOSE_WRITE = 0x00000008
    CLOSE_NOWRITE = 0x00000010
    OPEN = 0x00000020
    MOVED_FROM = 0x00000040
    MOVED_TO = 0x00000080
    CREATE = 0x00000100
    DELETE = 0x00000200
    DELETE_SELF = 0x00000400
    MOVE_SELF = 0x00000800
    OPEN_EXEC = 0x00001000

    Q_OVERFLOW = 0x00004000
    FS_ERROR = 0x00008000

    OPEN_PERM = 0x00010000
    ACCESS_PERM = 0x00020000
    OPEN_EXEC_PERM = 0x00040000

    EVENT_ON_CHILD = 0x08000000

    RENAME = 0x10000000

    ONDIR = 0x40000000

    CLOSE = CLOSE_WRITE | CLOSE_NOWRITE
    MOVE = MOVED_FROM | MOVED_TO

    # fanotify_init flags
    CLOEXEC = 0x00000001
    NONBLOCK = 0x00000002

    CLASS_NOTIF = 0x00000000
    CLASS_CONTENT = 0x00000004
    CLASS_PRE_CONTENT = 0x00000008

    UNLIMITED_QUEUE = 0x00000010
    UNLIMITED_MARKS = 0x00000020
    ENABLE_AUDIT = 0x00000040

    # event format flags
    REPORT_PIDFD = 0x00000080
    REPORT_TID = 0x00000100
    REPORT_FID = 0x00000200
    REPORT_DIR_FID = 0x00000400
    REPORT_NAME = 0x00000800
    REPORT_TARGET_FID = 0x00001000

    REPORT_DFID_NAME = REPORT_DIR_FID | REPORT_NAME
    REPORT_DFID_NAME_TARGET = REPORT_DFID_NAME | REPORT_FID | REPORT_TARGET_FID

    # fanotify_modify_mark flags
    MARK_ADD = 0x00000001
    MARK_REMOVE = 0x00000002
    MARK_DONT_FOLLOW = 0x00000004
    MARK_ONLYDIR = 0x00000008
    # MARK_MOUNT = 0x00000010
    MARK_IGNORED_MASK = 0x00000020
    MARK_IGNORED_SURV_MODIFY = 0x00000040
    MARK_FLUSH = 0x00000080
    # MARK_FILESYSTEM = 0x00000100
    MARK_EVICTABLE = 0x00000200
    MARK_IGNORE = 0x00000400 # not with MARK_IGNORED_MASK

    MARK_INODE = 0x00000000
    MARK_MOUNT = 0x00000010
    MARK_FILESYSTEM = 0x00000100

    MARK_IGNORE_SURV = MARK_IGNORE | MARK_IGNORED_SURV_MODIFY

    METADATA_VERSION = 3

    class event_metadata(ct.Structure) :
        _fields_ = \
            [
                ("event_len", ct.c_uint32),
                ("vers", ct.c_uint8),
                ("reserved", ct.c_uint8),
                ("metadata_len", ct.c_uint16),
                ("mask", ct.c_uint64),
                ("fd", ct.c_int32),
                ("pid", ct.c_int32),
            ]
    #end event_metadata

    EVENT_INFO_TYPE_FID = 1
    EVENT_INFO_TYPE_DFID_NAME = 2
    EVENT_INFO_TYPE_DFID = 3
    EVENT_INFO_TYPE_PIDFD = 4
    EVENT_INFO_TYPE_ERROR = 5

    EVENT_INFO_TYPE_OLD_DFID_NAME = 10 # for RENAME event
    # EVENT_INFO_TYPE_OLD_DFID = 11 # reserved
    EVENT_INFO_TYPE_NEW_DFID_NAME = 12
    # EVENT_INFO_TYPE_NEW_DFID = 13 # reserved

    class event_info_header(ct.Structure) :
        _fields_ = \
            [
                ("info_type", ct.c_uint8),
                ("pad", ct.c_uint8),
                ("len", ct.c_uint16),
            ]
    #end event_info_header

    class event_info_fid(ct.Structure) :
        pass
    event_info_fid._fields_ = \
        [
            ("hdr", event_info_header),
            ("fsid", kernel_fsid_t),
            ("handle", linuxfs.file_handle),
        ]
    #end event_info_fid

    class event_info_pidfd(ct.Structure) :
        pass
    event_info_pidfd._fields_ = \
        [
            ("hdr", event_info_header),
            ("pidfd", ct.c_int32),
        ]
    #end event_info_pidfd

    class event_info_error(ct.Structure) :
        pass
    event_info_error._fields_ = \
        [
            ("hdr", event_info_header),
            ("error", ct.c_int32),
            ("error_count", ct.c_uint32),
        ]
    #end event_info_error

    RESPONSE_INFO_NONE = 0
    RESPONSE_INFO_AUDIT_RULE = 1

    class response(ct.Structure) :
        _fields_ = \
            [
                ("fd", ct.c_int32),
                ("response", ct.c_uint32),
            ]
    #end response

    class response_info_header(ct.Structure) :
        _fields_ = \
            [
                ("type", ct.c_uint8),
                ("pad", ct.c_uint8),
                ("len", ct.c_uint16),
            ]
    #end response_info_header

    class response_info_audit_rule(ct.Structure) :
        pass
    response_info_audit_rule._fields_ = \
        [
            ("hdr", response_info_header),
            ("rule_number", ct.c_uint32),
            ("subj_trust", ct.c_uint32),
            ("obj_trust", ct.c_uint32),
        ]
    #end response_info_audit_rule

    ALLOW = 0x01
    DENY = 0x02
    AUDIT = 0x10
    INFO = 0x20

    NOPIDFD = NOFD = -1
    EPIDFD = -2

    EVENT_METADATA_LEN = ct.sizeof(event_metadata)

    # I think I might get rid of following adaptations of C macros ...

    def EVENT_NEXT(meta : bytes) :
        elt = (None, None) # to begin with (if not enough data available)
        if len(meta) >= FANOTIFY.EVENT_METADATA_LEN :
            event_len, = struct.unpack("=I", meta[:4])
            if event_len < FANOTIFY.EVENT_METADATA_LEN :
                raise ValueError("truncated fanotify event notification seen")
            elif event_len <= len(meta) :
                evtbytes = meta[:event_len]
                meta = meta[event_len:]
                evt = ct.cast(ct.cast(evtbytes, ct.c_void_p).value, ct.POINTER(FANOTIFY.event_metadata))
                if evt.contents.vers != FANOTIFY.METADATA_VERSION :
                    raise ValueError \
                      (
                            "unexpected fanotify metadata version %d, wanted %d"
                        %
                            (evt.contents.vers, FANOTIFY.METADATA_VERSION)
                      )
                #end if
                elt = (evt, evtbytes)
            #end if
        #end if
        return \
            elt, meta
    #end EVENT_NEXT

    def EVENT_OK(meta : bytes) :
        ok = False
        if len(meta) >= FANOTIFY.EVENT_METADATA_LEN :
            event_len, = struct.unpack("=I", meta[:4])
            if event_len < FANOTIFY.EVENT_METADATA_LEN :
                raise ValueError("truncated fanotify event notification received")
            elif event_len <= len(meta) :
                ok = True
            #end if
        #end if
        return \
            ok
    #end EVENT_OK

#end FANOTIFY

libc = ct.CDLL("libc.so.6", use_errno = True)

libc.fanotify_init.restype = ct.c_int
libc.fanotify_init.argtypes = (ct.c_uint, ct.c_int)
libc.fanotify_mark.restype = ct.c_int
libc.fanotify_mark.argtypes = (ct.c_int, ct.c_uint, ct.c_uint64, ct.c_int, ct.c_char_p)

#+
# Higher-level stuff
#-

class _BIT_FLAG(enum.Enum) :
    "subclasses have instances that are bit numbers or tuples" \
    " of them, but have a mask property which is the corresponding" \
    " bit mask."

    @property
    def mask(self) :
        "convert bit number(s) to mask."
        return \
            (
                lambda : 1 << self.value,
                lambda : reduce(operator.or_, (1 << v for v in self.value), 0),
            )[isinstance(self.value, tuple)]()
    #end mask

    @classmethod
    def make_set(celf, mask : int) :
        members = sorted \
          (
            celf.__members__.values(),
            key = lambda m : (lambda : 1, lambda : len(m.value))[isinstance(m.value, tuple)](),
            reverse = True
          )
        if members[-1].value == () and mask == 0 :
            result = {members[-1]}
        else :
            if members[-1].value == () :
                members = members[:-1]
            #end if
            remaining = mask
            removed = 0
            result = set()
            for m in members :
                if m.mask & remaining != 0 and m.mask & ~removed & remaining == m.mask & ~removed :
                    result.add(m)
                    remaining &= ~m.mask
                    removed |= m.mask
                #end if
            #end for
            if remaining != 0 :
                raise ValueError("undefined bits remaining in mask: %#08x" % remaining)
            #end if
        #end if
        return result
    #end make_set

#end _BIT_FLAG

class NOTIF_CLASS(_BIT_FLAG) :
    "which notification classes I can ask for. Specify only one."

    NOTIF = ()
    CONTENT = 2
    PRE_CONTENT = 3

#end NOTIF_CLASS

class NOTIF_FLAG(_BIT_FLAG) :
    "flags for fanotify_init excluding NOTIF, CONTENT and PRE_CONTENT."

    CLOEXEC = 0
    NONBLOCK = 1

    UNLIMITED_QUEUE = 4
    UNLIMITED_MARKS = 5
    ENABLE_AUDIT = 6

    REPORT_PIDFD = 7
    REPORT_TID = 8
    REPORT_FID = 9
    REPORT_DIR_FID = 10
    REPORT_NAME = 11
    REPORT_DFID_NAME = (10, 11)
    REPORT_TARGET_FID = 12
    REPORT_DFID_NAME_TARGET = (9, 10, 11, 12)

#end NOTIF_FLAG

class FD_BIT(_BIT_FLAG) :

    APPEND = 10
    CLOEXEC = 19
    DSYNC = 12
    RDONLY = ()
    RDWR = 1
    SYNC = (20, 12)
    WRONLY = 0

#end FD_BIT

class MARK_FLAG(_BIT_FLAG) :
    "MARK_xxx flags, excluding ADD, REMOVE and FLUSH."

    DONT_FOLLOW = 2
    ONLYDIR = 3
    MOUNT = 4
    IGNORED_MASK = 5
    IGNORED_SURV_MODIFY = 6
    FLUSH = 7
    FILESYSTEM = 8
    EVICTABLE = 9
    IGNORE = 10 # not with IGNORED_MASK
    INODE = ()
    IGNORE_SURV = (6, 10)

#end MARK_FLAG

class EVENT_FLAG(_BIT_FLAG) :

    ACCESS = 0
    MODIFY = 1
    ATTRIB = 2
    CLOSE_WRITE = 3
    CLOSE_NOWRITE = 4
    OPEN = 5
    MOVED_FROM = 6
    MOVED_TO = 7
    CREATE = 8
    DELETE = 9
    DELETE_SELF = 10
    MOVE_SELF = 11
    OPEN_EXEC = 12
    Q_OVERFLOW = 14
    FS_ERROR = 15
    OPEN_PERM = 16
    ACCESS_PERM = 17
    OPEN_EXEC_PERM = 18
    EVENT_ON_CHILD = 27
    RENAME = 28
    ONDIR = 30

    CLOSE = (3, 4)
    MOVE = (6, 7)

#end EVENT_FLAG

def get_file_handle(fidptr : ct.POINTER(FANOTIFY.event_info_fid)) :
    "assuming the arg is the address of a FANOTIFY.event_info_fid structure," \
    " returns a FileHandle object wrapping its handle field."
    return \
        (
            linuxfs.FileHandle.wrap
              (
                ct.cast
                  (
                        ct.cast(fidptr, ct.c_void_p).value
                    +
                        ct.sizeof(FANOTIFY.event_info_header)
                    +
                        ct.sizeof(kernel_fsid_t),
                    ct.POINTER(linuxfs.file_handle)
                  ),
                None
              )
        )
#end get_file_handle

def _get_flags_arg(arg, argname, bit_flag_class, exclude = 0, exclude_msg = None) :
    # decodes an argument which might be specified as either a set of elements
    # from some _BIT_FLAG subclass, or an integer; converts the former to the latter.
    # exclude is an integer bit mask for bits which must not be present if an
    # integer is specified; exclude_msg is the exception message if any such
    # bits are found.
    if isinstance(arg, (set, frozenset)) and all(isinstance(f, bit_flag_class) for f in arg) :
        arg = reduce(operator.or_, (f.mask for f in arg), 0)
    elif isinstance(arg, int) :
        if arg & exclude != 0 :
            if exclude_msg == None :
                exclude_msg = "invalid value for %s" % argname
            #end if
            raise ValueError(exclude_msg)
        #end if
    else :
        raise TypeError("invalid type for %s" % argname)
    #end
    return \
        arg
#end _get_flags_arg

class Notifier :
    "The main wrapper class for the fanotify(7) API. Do not instantiate" \
    " directly; use the create() classmethod to create a new instance."

    __slots__ = ("__weakref__", "fd", "_pending", "_buf", "_offset", "_nrbytes")

    _instances = WeakValueDictionary()

    def __new__(celf, fd) :
        self = celf._instances.get(fd)
        if self == None :
            self = super().__new__(celf)
            self.fd = fd
            self._pending = []
            self._buf = bytearray(4096 * (0,)) # as per recommendation in fanotify(7) man page
            self._offset = self._nrbytes = 0
            celf._instances[fd] = self
        #end if
        return \
            self
    #end __new__

    def fileno(self) :
        return \
            self.fd
    #end fileno

    @classmethod
    def create(celf, notif : NOTIF_CLASS, flags, event_f_flags) :
        "creates a new Notifier instance. Arguments are as to fanotify_init(2)," \
        " except that the CLASS_xxx bits are specified via the separate" \
        " notif arg."
        if not isinstance(notif, NOTIF_CLASS) :
            raise TypeError("notif must be a NOTIF_CLASS.xxx enum")
        #end if
        flags = _get_flags_arg \
          (
            arg = flags,
            argname = "flags",
            bit_flag_class = NOTIF_FLAG,
            exclude = FANOTIFY.CLASS_CONTENT | FANOTIFY.CLASS_PRE_CONTENT,
            exclude_msg = "specify notification class in separate notif arg"
          )
        event_f_flags = _get_flags_arg \
          (
            arg = event_f_flags,
            argname = "event_f_flags",
            bit_flag_class = FD_BIT
          )
        fd = libc.fanotify_init(notif.mask | flags, event_f_flags)
        if fd < 0 :
            errno = ct.get_errno()
            raise OSError(errno, os.strerror(errno))
        #end if
        return \
            celf(fd)
    #end create

    def __del__(self) :
        self.close()
    #end __del__

    def close(self) :
        if self.fd != None :
            os.close(self.fd)
            type(self)._instances.pop(self.fd, None)
            self.fd = None
        #end if
    #end close

    def _mark_add_remove(self, func, flags, mask, dirfd, pathname) :
        flags = _get_flags_arg \
          (
            arg = flags,
            argname = "flags",
            bit_flag_class = MARK_FLAG,
            exclude = FANOTIFY.MARK_ADD | FANOTIFY.MARK_REMOVE | FANOTIFY.MARK_FLUSH
          )
        mask = _get_flags_arg \
          (
            arg = mask,
            argname = "mask",
            bit_flag_class = EVENT_FLAG
          )
        if hasattr(dirfd, "fileno") :
            dirfd = dirfd.fileno()
        elif not isinstance(dirfd, int) :
            raise TypeError("invalid type for dirfd")
        #end if
        if isinstance(pathname, str) :
            pathname = pathname.encode()
        elif not isinstance(pathname, (bytes, bytearray)) and pathname != None :
            raise TypeError("invalid type for pathname")
        #end if
        if libc.fanotify_mark(self.fd, func | flags, mask, dirfd, pathname) < 0 :
            errno = ct.get_errno()
            raise OSError(errno, os.strerror(errno))
        #end if
    #end _mark_add_remove

    def add(self, flags, mask, dirfd, pathname) :
        "does a MARK_ADD operation on the Notifier."
        self._mark_add_remove(FANOTIFY.MARK_ADD, flags, mask, dirfd, pathname)
    #end add

    def remove(self, flags, mask) :
        "does a MARK_REMOVE operation on the Notifier."
        self._mark_add_remove(FANOTIFY.MARK_REMOVE, flags, mask, dirfd, pathname)
    #end remove

    def flush(self, flags) :
        "does a MARK_FLUSH operation on the Notifier."
        flags = _get_flags_arg \
          (
            arg = flags,
            argname = "flags",
            bit_flag_class = MARK_FLAG,
            exclude = FANOTIFY.MARK_ADD | FANOTIFY.MARK_REMOVE | FANOTIFY.MARK_FLUSH
          )
        only_flags = FANOTIFY.MARk_MOUNT | FANOTIFY.MARK_FILESYSTEM
        if flags & ~only_flags != 0 or flags & only_flags == only_flags :
            raise ValueError \
              (
                "invalid value for flags: specify no more than one of"
                " MARK_MOUNT or MARK_FILESYSTEM"
              )
        #end if
        if libc.fanotify_mark(self.fd, FANOTIFY.MARK_FLUSH | flags, 0, 0, None) < 0 :
            errno = ct.get_errno()
            raise OSError(errno, os.strerror(errno))
        #end if
    #end flush

    async def get_event(self, timeout = None) :

        read_done = None
        timing_out = None

        def reader() :
            assert len(self._pending) == 0
            tomove = self._nrbytes - self._offset
            if tomove > 0 :
                # move leftover data to front of buffer
                ct.memmove \
                  (
                    ct.byref(ct.c_ubyte.from_buffer(self._buf)),
                    ct.byref(ct.c_ubyte.from_buffer(self._buf, self._offset)),
                    tomove
                  )
            #end if
            self._nrbytes -= self._offset
            self._offset = 0
            toread = len(self._buf) - self._nrbytes
            assert toread > 0
            nrbytes = os.readv \
              (
                self.fd,
                ((toread * ct.c_ubyte).from_buffer(self._buf, self._nrbytes),)
              )
            assert nrbytes != 0, "EOF on notify!?"
            self._nrbytes += nrbytes
            got_one = False
            while True :
                try :
                    if self._nrbytes - self._offset < FANOTIFY.EVENT_METADATA_LEN :
                        break
                    evtptr = ct.pointer \
                      (
                        FANOTIFY.event_metadata.from_buffer(self._buf, self._offset)
                      )
                    event_len = evtptr.contents.event_len
                    if event_len < FANOTIFY.EVENT_METADATA_LEN :
                        raise ValueError("truncated fanotify event notification received")
                    #end if
                    if event_len + self._offset > self._nrbytes :
                        break
                    self._pending.append(evtptr)
                    self._offset += event_len
                    got_one = True
                except ValueError as bad :
                    read_done.set_exception(bad)
                    break
                #end try
            #end while
            if got_one and not read_done.done() :
                read_done.set_result(None)
            #end if
        #end reader

        def timed_out() :
            if not read_done.done() :
                read_done.set_result(None)
            #end if
        #end timed_out

    #begin get_event
        if len(self._pending) == 0 and (timeout == None or timeout > 0) :
            loop = asyncio.get_running_loop()
            loop.add_reader(self.fd, reader)
            read_done = loop.create_future()
            if timeout != None :
                timing_out = loop.call_later(timeout, timed_out)
            #end if
            try :
                await read_done
            finally :
                if timing_out != None :
                    timing_out.cancel()
                #end if
                try :
                    loop.remove_reader(self.fd)
                except ValueError :
                    pass
                #end try
            #end try
        #end if
        if len(self._pending) != 0 :
            evtptr = self._pending.pop(0)
            additional = []
            next = ct.cast(evtptr, ct.c_void_p).value + FANOTIFY.EVENT_METADATA_LEN
            remaining = evtptr.contents.event_len - FANOTIFY.EVENT_METADATA_LEN
            while remaining > 0 :
                infoptr = ct.cast(next, ct.POINTER(FANOTIFY.event_info_header))
                infolen = infoptr.contents.len
                if infolen < ct.sizeof(FANOTIFY.event_info_header) or infolen > remaining :
                    raise ValueError("truncated/bad fanotify info item received")
                #end if
                info_type = infoptr.contents.info_type
                infoptr = ct.cast \
                  (
                    infoptr,
                    ct.POINTER
                      (
                        {
                            FANOTIFY.EVENT_INFO_TYPE_FID : FANOTIFY.event_info_fid,
                            FANOTIFY.EVENT_INFO_TYPE_DFID_NAME : FANOTIFY.event_info_fid,
                            FANOTIFY.EVENT_INFO_TYPE_DFID : FANOTIFY.event_info_fid,
                            FANOTIFY.EVENT_INFO_TYPE_PIDFD : FANOTIFY.event_info_pidfd,
                            FANOTIFY.EVENT_INFO_TYPE_ERROR : FANOTIFY.event_info_error,
                        }[info_type]
                      )
                  )
                additional.append(infoptr)
                if info_type == FANOTIFY.EVENT_INFO_TYPE_DFID_NAME :
                    nameptr = ct.cast \
                      (
                            ct.cast(infoptr, ct.c_void_p).value
                        +
                            ct.sizeof(FANOTIFY.event_info_fid)
                        +
                            infoptr.contents.handle.handle_bytes,
                        ct.c_char_p
                      )
                    additional.append(nameptr.value)
                #end if
                next += infolen
                remaining -= infolen
            #end while
            result = (evtptr, additional)
        else :
            result = None
        #end if
        return \
            result
    #end get_event

#end Notifier

#+
# Cleanup
#-

def _atexit() :
    # disable all __del__ methods at process termination to avoid segfaults
    for cls in (Notifier,) :
        delattr(cls, "__del__")
    #end for
#end _atexit
atexit.register(_atexit)
del _atexit
